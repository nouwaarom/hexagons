project(hexagons)

add_executable(hexagonize hexagons.c)

target_link_libraries(hexagonize PRIVATE m c)