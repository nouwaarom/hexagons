#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

typedef struct {
	uint8_t* data;
	int width;
	int height;
	int depth;
} image_t;

typedef struct {
    float r;
    float g;
    float b;
} pixel;

int* get_row_indexes(int row_index, int* previous_start_index_p, int* start_index_p, int grid_width)
{
    int* indexes = (int*)malloc(grid_width*sizeof(int)); 
    int previous_start_index = *previous_start_index_p;
    int start_index = *start_index_p;

    int step = row_index & 0x1;
    for (int i = 0; i < grid_width; i++) {
        if (0 == step) {
            indexes[i] = previous_start_index++;
            step = 1;
        } else {
            indexes[i] = start_index++;
            step = 0;
        }
    }

    // Update the index pointers.
    *previous_start_index_p = *start_index_p;
    *start_index_p = start_index;

    return indexes;
}

unsigned char* blur_hexagon(const image_t image, int edge_size) {
    float row_height = sqrtf(3.0f)*(float)edge_size;
    int grid_height = (int)ceilf((float)image.height/row_height);

    // A represents the inter hexagon gridsize.
    float a = (float)edge_size/2.0f;
    float row_width = 3.0f*a;
    int grid_width = (int)ceilf((float)image.width/(float)edge_size);

    int idx = 0;
    int grid_x, grid_y;
    int previous_index[1];
    int current_index[1];
    int* row_indexes = NULL;

    // Initialize bins.
    pixel* bins = (pixel*)malloc(grid_height*grid_width*sizeof(pixel));
    for (int i = 0; i < grid_width*grid_height; i++) {
        bins[i].r = 0;
        bins[i].g = 0;
        bins[i].b = 0;
    }

    printf("Creating a grid of: %d, %d\n", grid_width, grid_height);
    
    // Generate all rows.
    previous_index[0] = 0;
    current_index[0] = 0;
    grid_y = -1;
    // Average over the input image.
    for (int y = 0; y < image.height; y++) {
        if ((float)grid_y * row_height < (float)y) {
			free(row_indexes);
            row_indexes = get_row_indexes(grid_y++, previous_index, current_index, grid_width);
        }

        grid_x = 0;
        for (int x = 0; x < image.width; x++) {
            if ((float)grid_x * row_width < (float)x) {
                idx = row_indexes[++grid_x];
            }
            bins[idx].r += (float)image.data[3*(y*image.width+x)];
            bins[idx].g += (float)image.data[3*(y*image.width+x)+1];
            bins[idx].b += (float)image.data[3*(y*image.width+x)+2];
        }
    }

    // Rescale the values in the bins.
    for (int i = 0; i < grid_width*grid_height; i++) {
        bins[i].r = bins[i].r/(row_width*row_height*2);
        bins[i].g = bins[i].g/(row_width*row_height*2);
        bins[i].b = bins[i].b/(row_width*row_height*2);
    }

    // Write the output image based on the averaged data. 
    unsigned char* output = (unsigned char*)malloc(3*image.width*image.height*sizeof(unsigned char));
    previous_index[0] = 0;
    current_index[0] = 0;
    grid_y = -1;
    for (int y = 0; y < image.height; y++) {
        if ((float)grid_y * row_height < (float)y) {
			free(row_indexes);
            row_indexes = get_row_indexes(grid_y++, previous_index, current_index, grid_width);
        }

        grid_x = 0;
        for (int x = 0; x < image.width; x++) {
            if ((float)grid_x * row_width < (float)x) {
                idx = row_indexes[++grid_x];
            }
            output[3*(y*image.width+x)]   = (uint8_t)bins[idx].r;
            output[3*(y*image.width+x)+1] = (uint8_t)bins[idx].g;
            output[3*(y*image.width+x)+2] = (uint8_t)bins[idx].b;
        }
    }

	free(row_indexes);

    return output;
}

int main(int argc, char** argv) {
	if (argc != 3) {
		fprintf(stderr, "Required arguments are: <input_file> <grid_size>\n");
		return -1;
	}

	char* input_file = argv[1];
	int grid_size = atoi(argv[2]);
	if (grid_size == 0 || grid_size > 500) {
		fprintf(stderr, "Invalid grid size: %d\n", grid_size);
		return -1;
	}

	printf("Convert %s to hexagons with a gridsize of %d\n", input_file, grid_size);

    int image_size[3] = {1920, 1080, 3};

    unsigned char* data = stbi_load(input_file, &image_size[0], &image_size[1], &image_size[3], 3);
    if (data == 0) {
        return -1;
    }

	image_t image = { .data = data, .width = image_size[0], image_size[1], image_size[2] };

    printf("Loaded image of size %d by %d with %d channels.\n", image.width, image.height, image.depth);

    uint8_t* blurred = blur_hexagon(image, grid_size);

	printf("Storing result in out.jpg\n");

    stbi_write_jpg("out.jpg", image.width, image.height, image.depth, blurred, 70);

	printf("Done\n");

	free(blurred);
    stbi_image_free(data);

    return 0;
}
